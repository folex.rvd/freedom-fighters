# Hiperlokális közösségi háló

## Célja

Egy nem-hiperlokális portállal szemben a cél publikus, de kisközösségben releváns reputáció építése (nyilvántartott kalákakör) és az ismerkedés. Ha "tudom hol lakik", akkor jobban beengedek egy "idegent" is takarítani a lakásába, illetve a kölcsönösség kikényszeríti a "jó munkát".

A "globális" jó tevéssel mindig az a baj, hogy még ha mindkét fél jó szándékú, akkor is "elsodorja az élet" egymástól őket és/vagy jóhiszeműen is elfelejtődik. Viszont ha valakit napi szinten látunk, ez nem történhet meg. Emiatt a hiperlokalitás kiterjeszthető lehet munkahelyekre és más épülettömbökre is.

* https://en.wikipedia.org/wiki/Hyperlocal

### Kötődési szintek

* információmegosztás
* emberek közti kíváncsiság
* szórakozás, közösségépítés
* szívességkör

## Léptékei

### Nagyságrendek

* lépcsőház, társasház, munkahely
* lakótelep-háztömb, munkahely szomszédsága
* utca, összetartó falusi környék
* városi kerületrész

### Példák baráti-jószomszédi léptékű interakcióra

* gift economy
  * a kalákakörnél egyszerűbb, mivel ebben nem kell nyilvántartani a szívességeket, hanem a résztvevők adaptív visszacsatolással modulálják a kínált szívességeik elsütési rátáját a kapott szívességek rátájával arányban, valamelyest egy neurális háló mintájára
  * https://en.wikipedia.org/wiki/Gift_economy
  * pszichológiailag valószínűleg akkor a legjobb az ajándékozás amennyiben privát, tehát ha ellentételezésként még a dicsőségfalon sem jár érte hely

### Példák összetartó lakóközösségi léptékű interakcióra

* közgyűlések
* takarítás
* összejövetelek (bulik, grillezés, temetés, esküvő)
* határidők
* közös költség összege
* kertészkedés tervezés (produktum elosztása), kivitelezés, karbantartási feladatok (komposztáló keverés, fűnyírás, trágyázás, palántázás, locsolás)
* benti növényzet kialakítása, cserepes virágok tulajdonlása vagy locsolási rendje
* fogyó eszközök, munkaeszközök
* postás
* szórólapok
* szerelőt többen egyszerre beszervezni
* ingyen bevásárlás
* kölcsönkérni: adathordozó, alap élelmiszer, gyógyszer
* vészhelyzet: félrenyelt, elesett, csőtörés, gázszivárgás, épületszerkezeti hiba
* összefogások (pl. internet szolgáltató váltás)
* lakás eladó-kiadó
* közös tulajdon dekorációja
* Ki parkolt a helyemre az autójával? Ki pakolt a lépcsőházba, ajtó elé?

### Példák utca léptékű interakcióra

* elkapta a szél az antennát, tetőcserepet, virágcserepet, ágat
* vagyonbiztonság
  * szól az autó- vagy lakásriasztó
  * nyitva maradt az ajtó
  * kitört az ablak
  * ki vannak teregetve a ruhák és jön a vihar
  * Hívjuk-e a 112-t? Mi az amit látok vagy hallok (feltörik az autót, ablakot, ajtót, lövöldözés, sírás, veszekedés)?
* nálatok sincs víz/villany/internet? Közműszünet alatt: xy óra-perckor lesz átmenetileg központi aggregátorról áram x órára.
* patakzik a víz az úton (csőtörés, tűzcsap)
* elromlott utcabútor
* nálatok sem vitték el a kukát?
  * szelektív- vagy zöldhulladék-gyűjtési szabályok változása
* hozzátok is jár a mókus vagy nyest?
* mi ez a hangzavar, akár az éjszaka közepén (nyest, bagoly, macskák, esetleg a saját macskánkat marcangolják a kutyák)
  * egész éjszaka ugat valakinek a kutyája, kéretik kezelni a problémát (TODO: hangfelismeréssel akár automatizálható a tulajdonos felkeresése)
* utólag érdeklődni a közelmúltban tapasztalt szokatlan események okáról (kit vitt el a mentő, rendőr, milyen ügyben jöttek űrruhások, mi lett a szokásos postással)
* zajjal járó munkálatok előzetes bejelentése
* költségmegosztás
  * postaköltség megosztása ha messziről rendel valaki valamit
  * telekocsi
  * sittes konténer rendelése
  * kötegelve végezhető, rövid ideig tartó szakmunkák, mint a parkolóhely festés
* zsebpénzes munkák vagy szívességkör (takarítás, kutyasétáltatás, bevásárlás, autómosás, segítség összeszereléshez, kerti munkák, segédmunka, gyermekfelügyelet)
* lomtalanítás
* megmaradt építőanyagok elvihetők
* csere-bere, könyvek

## Megvalósítás

### Lehetséges modalitások

(TODO: melyek ezek közül a legelemibbek amivel érdemes kezdeni?)

* elektronikus faliújság feltűzhető elévülő rövid üzenetekkel, értesítéssel a változásokról
* dicsőségfal az elvégzett tettekkel
* szavazás
* névtelen ötletláda
* moderált panaszláda
* szöveges csevegőszobák
* levelezőlista
* fórum
* kerekasztal beszélgetések
* helyi webrádió saját műsorokkal
* kattintható térkép a lakók elérhetőségével

### Technológia

Csak akkor igazán jó alternatíva ha kivétel nélkül mindenki részt fog venni benne egy adott közösségen belül.

* Internethez köthető
  * A közösség egy önkéntese által üzemeltetett
  * Közös költségből fizetett központi infrastruktúrát igénybe vevő
* Előfizetés nélkül is használható
  * Delay tolerant networking, Bluetooth piconet, wifi P2P Direct, mesh
* Legyen biztosítható nagyon olcsó kliens eszköz is a számítógépes írástudás jegyében
